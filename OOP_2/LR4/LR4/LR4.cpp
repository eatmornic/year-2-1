#include"stdafx.h"
#include <iostream>
using namespace std;

class aircraft
{
private:
    void message();
    friend class airfabrique;
protected:
    char type[10];
    int speed;
public:
    void in();
    void out();
};

class airfabrique
{
public:
    void aircraftMessage(aircraft *ar1);
};

class transport
{
protected:
    int tonnage;
public:
    void in();
    void out();
};

class shuttle :aircraft, transport
{
public:
    void in();
    void out();
};

void aircraft::message()
{
    cout << "class aircraft message" << endl;
}

void aircraft::in()
{
    cout << "Please, enter type of aircraft: ";
    cin >> type;
    cout << "Please, enter speed of aircraft: ";
    cin >> speed;
}

void aircraft::out()
{
    cout << type << endl << speed << endl;
}

void airfabrique::aircraftMessage(aircraft *ar1)
{
    ar1->message();
}

void transport::in()
{
    cout << "Please, enter tonnage: ";
    cin >> tonnage;
    cout << endl;
}

void transport::out()
{
    cout << tonnage << endl;
}

void shuttle::in()
{
    aircraft::in();
    transport::in();
    
    cout << endl;
}

void shuttle::out()
{
    
    aircraft::out();
    transport::out();
}

void printMenu()
{
    cout << "0.exit\n1.baseClassInputOutput\n2.derrivedClassInputOutput\n";
}

int main()
{
    int i;
    aircraft *ar1 = new aircraft;
    transport *tr1 = new transport;
    shuttle *sh1 = new shuttle;
    airfabrique *fb1 = new airfabrique;
    do
    {
        printMenu();
        cin >> i;
        switch (i)
        {
            case 1:
                cout << "Aircraft:\n";
                ar1->in();
                fb1->aircraftMessage(ar1);
                
                cout << "Transport:\n";
                tr1->in();
                
                ar1->out();
                tr1->out();
                break;
            case 2:
                cout << endl << "Shuttle: \n";
                sh1->in();
                sh1->out();
                break;
        }
    } while (i != 0);
    
    delete ar1, tr1, sh1;
    return 0;
}

