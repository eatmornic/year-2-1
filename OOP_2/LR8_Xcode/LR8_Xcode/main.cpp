//
//  main.cpp
//  LR8_Xcode
//
//  Created by Дмитрий Пархоменко on 21.12.2017.
//  Copyright © 2017 Дмитрий Пархоменко. All rights reserved.
//

#include <iostream>
#include <fstream>
#include <bitset>   //вывод двоичного кода
#include <cctype>
#include <math.h>

using namespace std;

class numb
{
public:

    int calcFibonacciForN(int n)
    {
        if (n==1 || n==2) return 1;
        return calcFibonacciForN(n-2) + calcFibonacciForN(n-1);
    }
    
    void calcFunc(int x)
    {
        try
        {
            if (x == 0) throw '0';
            if (x!=0) throw 0;
        }
        catch (char) {
            cout<<"На ноль делить нельзя"<<endl;
        }
        catch(int){
            cout << log(1/atan(x)) << endl;
        }
    }
};
/* доп задание
class Error
{
    int n;
};
class IntError:Error
{
    int m;
};*/

int main()
{
    numb n1;
    
    cout<<"Enter N: ";
    int nmb;
    cin >> nmb;
    int fibN1 = n1.calcFibonacciForN(nmb);
        
    cout << nmb << " = fib hex " << hex << fibN1 <<endl;
    ofstream fout("file.txt",ios::app);
    fout << "n = "<< nmb << endl << "fib hex = " << hex << fibN1 << endl << "fib oct = " << oct << fibN1 << endl << "fib bin = " << bitset<sizeof(fibN1)*8>(fibN1) << endl;
    fout.close();
    
    cout<<"Enter Х: ";
    cin >> nmb;
    n1.calcFunc(nmb);
    
    /*доп задание
    Error err1;
    IntError err2;
    
    cout<<"Введите n:"<<endl;
    int n;
    cin >> n;
    
    
    try {
        if (n==0) {
            throw err1;
        }
        if (n==1) {
            throw err2;
        }
    }
    catch (IntError) {
        cout << "INTError"<<endl;
    }
    catch (Error) {
        cout << "Error"<<endl;
    }*/
    return 0;
}
