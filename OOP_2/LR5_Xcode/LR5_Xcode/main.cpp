//
//  main.cpp
//  LR5_Xcode
//
//  Created by Дмитрий Пархоменко on 08.11.2017.
//  Copyright © 2017 Дмитрий Пархоменко. All rights reserved.
//

#include <iostream>
using namespace std;
class MatrixReal;
class MatrixInteger {
    int matrix[3][3] = {
        {0,5,6},
        {0,3,9},
        {2,1,0}
    };
    friend void Op(MatrixInteger int1, MatrixReal real1);    //объявление дружественной функции
    
public:
    MatrixInteger() {
        for (size_t i = 0; i<3; i++) {
            for (size_t j = 0; j<3; j++) {
                matrix[i][j]++;
            }
        }
    }
    MatrixInteger(int d) {
        for (size_t i = 0; i<3; i++) {
            for (size_t j = 0; j<3; j++) {
                matrix[i][j] = matrix[i][j] + d;
            }
        }
    }
    
    
    void out() {
        for (size_t i = 0; i<3; i++) {
            for (size_t j = 0; j<3; j++) {
                cout << matrix[i][j] << "   ";
            }
            cout << endl;
        }
    }
};

class MatrixReal {
    double matrix[3][3] = {
        {3,5,6},
        {0,4,9},
        {2,1,7}
    };
public:
    MatrixReal() {
        for (size_t i = 0; i<3; i++) {
            for (size_t j = 0; j<3; j++) {
                matrix[i][j] = matrix[i][j]/2;
            }
        }
    }
    MatrixReal(int d) {
        for (size_t i = 0; i<3; i++) {
            for (size_t j = 0; j<3; j++) {
                matrix[i][j] = matrix[i][j] + d;
            }
        }
    }
    
    friend void Op(MatrixInteger int1, MatrixReal real1);    //объявление дружественной функции
    void out() {
        for (size_t i = 0; i<3; i++) {
            for (size_t j = 0; j<3; j++) {
                cout << matrix[i][j] << "   ";
            }
            cout << endl;
        }
    }
};

void Op(MatrixInteger int1, MatrixReal real1) {             //определение дружественной функции
    double opMatrixInt=0, opMatrixReal=0;
    
    for (size_t i = 0; i<3; i++) {
        opMatrixInt = opMatrixInt + int1.matrix[i][i];
    }
    
    for (size_t i = 0; i<3; i++) {
        opMatrixReal = opMatrixReal + real1.matrix[i][i];
    }
    
    if (opMatrixInt > opMatrixReal) {
        cout << "Произведение диагональных элементов матрицы 1 больше суммы диагональных элементов матрицы 2";
    }
    else {
        if (opMatrixInt == opMatrixReal) {
            cout << "Произведение диагональных элементов матрицы 1 равно сумме диагональных элементов матрицы 2";
        }
        else {
            cout << "Произведение диагональных элементов матрицы 1 меньше суммы диагональных элементов матрицы 2";
        }
    }
    cout << endl;
}

int main() {
    
    MatrixInteger int1;
    int1.out();
    cout << endl;
    
    MatrixReal real1;
    real1.out();
    cout << endl;
    
    Op(int1, real1);               //вызов дружественной функции
    
    return 0;
}
