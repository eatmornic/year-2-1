#include "stdafx.h"
#include <iostream>
using namespace std;

class Matrix
{
protected:
	int mass[3][3] = {{-1, -2, 3}, {-54, -5, 6}, {7, -8, 0}};
	virtual void printMass() = 0;
public:
	void doPrint();
};

class Matrix1 : public Matrix
{
protected:
	void printMass();
};

class Matrix2 :public Matrix
{
protected:
	int pos[9], neg[9], boolPos = 0, boolNeg = 0, kolPos = 0, kolNeg = 0, kolNull = 0;
	
	void printMass();
public:
	Matrix2();		//конструктор задания начальных зничений для конечных массивов

};


void Matrix::doPrint() 
{	
	printMass();
}



void Matrix1::printMass()
{
	int i1, i0;
	for (i0 = 0; i0 < 3; i0++)
	{
		for (i1 = 0; i1 < 3; i1++)
		{
			cout << mass[i0][i1] << " ";
		}
		cout << endl;
	}
	cout << endl;
}

Matrix2::Matrix2()
{
	int i;
	for (i = 0; i < 9; i++)
	{
		pos[i] = 0;
		neg[i] = 0;
	}
}

void Matrix2::printMass()
{

	int i1, i0, i2;
	for (i0 = 0; i0 < 3; i0++)
	{
		for (i1 = 0; i1 < 3; i1++)
		{
			cout << mass[i0][i1] << " ";
		}
		cout << endl;
	}
	cout << endl;

	i1, i0, i2 = 0;

	for (i0 = 0; i0 < 3; i0++)		//присвоение значений 
	{
		for (i1 = 0; i1 < 3; i1++)
		{
			if ((mass[i0][i1] < 0) && (i2 < 9))
			{
				neg[i2] = mass[i0][i1];
				i2 = i2 + 1;
				boolNeg = 1;
			}
			else
			{
				if ((mass[i0][i1] == 0) && (i2 < 9))
				{
					kolNull++;
					i2 = i2 + 1;
				}
				else
				{
					pos[i2] = mass[i0][i1];
					i2 = i2 + 1;
					boolPos = 1;
				}

			}
		}
	}
	if (boolPos == 1)			//выввод положительных значений
	{
		cout << "Polozhitelnie znacheniya: \n";
		for (i2 = 0; i2 < 9; i2++)
		{
			if (pos[i2] > 0)
			{
				cout << pos[i2] << ", ";
				kolPos++;
			}
		}
		cout << endl << "kolichestvo znacheniy: " << kolPos << endl << endl;
	}

	if (boolNeg == 1)			//выввод отрицательных значений
	{
		cout << "Otrizatelnie znacheniya: \n";
		for (i2 = 0; i2 < 9; i2++)
		{
			if (neg[i2] < 0)
			{
				cout << neg[i2] << ", ";
				kolNeg++;
			}
		}
		cout << endl << "kolichestvo znacheniy: " << kolNeg << endl << endl;
	}
	cout << "kolichestvo nulevih znacheniy: " << kolNull;
}

void f(Matrix *ptr)
{
	ptr->doPrint();
}

int main()
{
	Matrix1 *ptr1;
	Matrix2 *ptr2;
	ptr1 = new Matrix1;
	ptr2 = new Matrix2;

	cout << "vivod matrix1:\n";
	f(ptr1);

	cout << "vivod matrix2:\n";
	f(ptr2);

	int i;
	cin >> i;

	delete ptr1, ptr2;
    return 0;
}