// LR6_VisualStudio.cpp : Defines the entry point for the console application.
//

#include <iostream>
#include <string>
using namespace std;

class Time {
    int hours, minutes, seconds;
public:
    Time() { hours, seconds, minutes = 0; }
    Time(int hours, int minutes, int seconds) {
        this->hours = hours;
        this->minutes = minutes;
        this->seconds = seconds;
    }
    
    void checkTime() {
        if (seconds < 0) {
            seconds += 60;
            minutes -= 1;
        }
        if (minutes < 0) {
            minutes += 60;
            hours -= 1;
        }
        if (hours < 0) {
            hours += 24;
            cout << "Move to past date" << endl;
        }
        
        if (seconds >= 60) {
            seconds -= 60;
            minutes += 1;
        }
        if (minutes >= 60) {
            minutes -= 60;
            hours += 1;
        }
        if (hours >= 24) {
            hours = 0;
            cout << "Move to future date" << endl;
        }
    }
    
    void get() { cout << hours << ':' << minutes << ':' << seconds << endl; }
    
    void operator--() {
        seconds -= 5;
        checkTime();
        get();
    }
    
    friend void operator++ (Time T1);
    friend void operator+ (Time T1, Time T2);
    friend void operator!= (Time T1, Time T2);
};

void operator++ (Time T1) {
    T1.seconds += 5;
    T1.checkTime();
    
    T1.get();
}

void operator+ (Time T1, Time T2) {
    T1.seconds += T2.seconds;
    T1.checkTime();
    T1.minutes += T2.minutes;
    T1.checkTime();
    T1.hours += T2.hours;
    T1.checkTime();
    
    T1.get();
}


int main() {
    Time t1(00, 00, 04);
    Time t2(22, 59, 59);
    
    t1.get();
    cout << endl;
    --t1;
    ++t1;
    
    t1 + t2;
    
    system("pause");
}

