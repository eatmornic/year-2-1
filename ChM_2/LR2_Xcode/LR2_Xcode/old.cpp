//
//  main.cpp
//  LR2_Xcode
//
//  Created by Дмитрий Пархоменко on 09.11.2017.
//  Copyright © 2017 Дмитрий Пархоменко. All rights reserved.
//

#include <iostream>
#include <iomanip>          //для вывода огр кол знаков после запятой (cout << setprecision(3) << x;)

using namespace std;

int main()
{
    /* CОЗДАНИЕ ПЕРЕМЕННЫХ */
    float **ptr_massL = new float*[5];
    float **ptr_massU = new float*[5];
    float *ptr_massB = new float[5];
    for (size_t i = 0; i<5; i++)
    {
        ptr_massL[i] = new float[5];
        ptr_massU[i] = new float[5];
    }
    
    float matrixL[5][5] =
    {
        {1, 0, 0, 0, 0},
        {0.55, 1, 0, 0, 0},
        {0.66, 0.958, 1, 0, 0},
        {0.27, 0.267, 0.642, 1, 0},
        {1, -0.267, -0.642, -0.853, 1}
    };
    float matrixU[5][5] =
    {
        {3.6, -2, -13.3, 0.4, 0.5},
        {0, 5.33, 11.9, -1.07, -0.5},
        {0, 0, -8.5, -10.3, 1.8},
        {0, 0, 0, 7.6, -2.4},
        {0, 0, 0, 0, -4.7}
    };
    string matrixB = {1, 2, -3, -1, 3};
    
    for (size_t i = 0; i<5; i++)
    {
        for (size_t j = 0; j<5; j++) {
            ptr_massL[i][j] = matrixL[i][j];
            ptr_massU[i][j] = matrixU[i][j];
        };
        ptr_massB[i] = matrixB[i];
    };
    
    /* конечные матрицы */
    float *ptr_massX = new float[5];
    float *ptr_massY = new float[5];
    
    
    /* РАБОТА С МАТРИЦАМИ */
    
    for (size_t i=0; i<5; i++)  //L * y = b
    {
        float S = 0;
        for (size_t j = 0; j<i; j++) {
            S = S + ptr_massL[i][j] * ptr_massY[j];
        }
        ptr_massY[i] = ptr_massB[i] - S;
    }
    
    for (size_t i=0; i<5; i++)  //U * x = y
    {
        float S = 0;
        for (size_t j = 0; j<i; j++) {
            S = S + ptr_massU[i][j] * ptr_massX[j];
        }
        ptr_massX[i] = ptr_massY[i] / ptr_massU[i][i] - S;
    }
    
    cout << "y:" << endl;
    for (size_t i=0; i<5; i++)
    {
        cout << ptr_massY[i] << endl;
    }
    
    cout << endl << "x:" << endl;
    for (size_t i=0; i<5; i++)
    {
        cout << ptr_massX[i] << endl;
    }
    
    /* УДАЛЕНИЕ ПЕРЕМЕННЫХ */
    for (size_t i = 0; i<5; i++)
    {
        delete[]ptr_massL[i];
        delete[]ptr_massU[i];
    }
    delete [] ptr_massL, ptr_massU, ptr_massB, ptr_massX, ptr_massY;
    
    return 0;
}
