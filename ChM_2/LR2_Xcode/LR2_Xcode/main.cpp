//#include "stdafx.h"         для visual studio
#include <iostream>
#include <iomanip>          //для вывода огр кол знаков после запятой (cout << setprecision(3) << x;)
#include <cmath>            //для xcode
using namespace std;


void outMass(int *ptr_size, float **ptr_mass);      //вывод значений динамической матрицы
void fillMassL(float **massL, int *size);           //заполнение по диагонали матрицы L
void absMaxValU(int *ptr_size, float **ptr_massU, float *ptr_highVal, int *ptr_lineVal, size_t j);      //нахождение абсолютно максимального числа
void replaceStr(float **mass, int size, float maxVal, int lineVal, int j);  //замена строк у ТЕКУЩЕЙ матрицы U
void fillCoofMassL(int *ptr_size, float **ptr_massL, float **ptr_massU,size_t j);   //вычисл. коэф. матрицы L
void fillMassU(int *ptr_size, float **ptr_massU, float **ptr_massL, size_t j);  //замена элементов U матрицы


int main()
{
//#1
/* ИНИЦАЛИЗАЦИЯ ДИНАМИЧЕСКИХ ПЕРЕМЕННЫХ */
    int *ptr_size = new int;
    cout << "Введите размерность матрицы: ";
    cin >> *ptr_size;
    
    float **ptr_mass = new float*[*ptr_size];                                               //начальная матрица
    float **ptr_massU = new float*[*ptr_size];                                              //U-матрица
    float **ptr_massL = new float*[*ptr_size];                                              //L-матрица
    
    for (int i = 0; i < *ptr_size; i++)
    {
        ptr_mass[i] = new float[*ptr_size];
        ptr_massU[i] = new float[*ptr_size];
        ptr_massL[i] = new float[*ptr_size];
    }
    
    cout << "Введите данные матрицы: \n";
    for (int i = 0; i < *ptr_size; i++)                                                     //присваивание значений динамическим переменным
    {
        cout << "Строка "<< i+1 << endl;
        for (int j = 0; j < *ptr_size; j++)
        {
            cin >> ptr_mass[i][j];
            ptr_massU[i][j] = ptr_mass[i][j];
        }
    }
    
    fillMassL(ptr_massL, ptr_size);                                                         //заполнение диагонали L-матрицы единицами и нулями
    
    
/* РАБОТА С ТАБЛИЦЕЙ */
    cout << "Введенная матрица: \n";
    outMass(ptr_size, ptr_mass);
    cout << endl;
    
// #1
    for (size_t i = 0; i < *ptr_size; i++)                                                  //столбец
    {
        if (i!=*ptr_size-1)
        {
            cout << "Действие #" << i+1;
        } else cout << "Результат:";
        
        
        float *ptr_highVal = new float;                                                     //матрица наиб значений в солбцах
        int *ptr_lineVal = new int;                                                         //строка этого значения
        *ptr_highVal = *ptr_lineVal = 0;
        
        absMaxValU(ptr_size, ptr_massU, ptr_highVal, ptr_lineVal, i);
        replaceStr(ptr_massU, *ptr_size, *ptr_highVal, *ptr_lineVal, i);                    //замена строк
        fillCoofMassL(ptr_size,ptr_massL,ptr_massU,i);                                      //вычисл. коэф. матрицы L
        fillMassU(ptr_size,ptr_massU,ptr_massL, i);                                         //замена элементов U матрицы
        delete ptr_highVal, ptr_lineVal;
        
        cout << "\nL-matrix:\n";
        outMass(ptr_size, ptr_massL);
        cout << "U-matrix:\n";
        outMass(ptr_size, ptr_massU);
        cout << endl;
    }
    
    
// #2
    /* вектор b */
    float *ptr_massB = new float[*ptr_size];
    cout << "Введите вектор b:" << endl;
    for (size_t i = 0; i<*ptr_size; i++) cin >> ptr_massB[i];
    
    /* конечные матрицы */
    float *ptr_massX = new float[*ptr_size];
    float *ptr_massY = new float[*ptr_size];
    
    
    /* работа с матрицами */
    for (size_t i=0; i<*ptr_size; i++)  //L * y = b
    {
        float S = 0;
        for (size_t j = 0; j<i; j++)S = S + ptr_massL[i][j] * ptr_massY[j];
        ptr_massY[i] = ptr_massB[i] - S;
    }
    
    for (size_t i=0; i<*ptr_size; i++)  //U * x = y
    {
        float S = 0;
        for (size_t j = 0; j<i; j++) S = S + ptr_massU[i][j] * ptr_massX[j];
        ptr_massX[i] = ptr_massY[i] / ptr_massU[i][i] - S;
    }
    
        
    cout << "\nРешение:\n" << "y:" << endl;
    for (size_t i=0; i<*ptr_size; i++)
    {
        cout << ptr_massY[i] << endl;
    }
    
    cout << endl << "x:" << endl;
    for (size_t i=0; i<*ptr_size; i++)
    {
        cout << ptr_massX[i] << endl;
    }
    
    
/* УДАЛЕНИЕ ДИНАМИЧЕСКИХ ПЕРЕМЕННЫХ */
    for (int i = 0; i < *ptr_size; i++)
    {
        delete[] ptr_mass[i];
        delete[] ptr_massU[i];
        delete[] ptr_massL[i];
    }
    delete[] ptr_mass, ptr_massU, ptr_massL, ptr_massB, ptr_massX, ptr_massY;
    delete ptr_size;

    return 0;
}


void outMass(int *ptr_size, float **ptr_mass)                                               //вывод значений динамической матрицы
{
    for (int i = 0; i < *ptr_size; i++)
    {
        for (int j = 0; j < *ptr_size; j++)
        {
            if (ptr_mass[i][j] >= 0) cout << "  ";
            cout << setprecision(3) << ptr_mass[i][j] << "  ";
        }
        cout << endl;
    }
}

void fillMassL(float **massL, int *size)                                                    //заполнение по диагонали матрицы L
{
    for (size_t j = 0; j < *size; j++)
    {
        for (size_t i = 0; i < *size; i++)
        {
            if (i == j) massL[j][i] = 1;
            else massL[j][i] = 0;
        }
    }
}

void absMaxValU(int *ptr_size, float **ptr_massU, float *ptr_highVal, int *ptr_lineVal, size_t j)       //нахождение абсолютно максимального числа
{
    for (size_t i = j; i < *ptr_size; i++)
    {
        if ((abs(ptr_massU[i][j])) > abs(*ptr_highVal))
        {
            *ptr_highVal = ptr_massU[i][j];
            *ptr_lineVal = i;
        };
    }
}

void replaceStr(float **mass, int size, float maxVal, int lineVal, int j)                   //замена строк у ТЕКУЩЕЙ матрицы U
{    /*(массив, размер массива, максимальное значение в позиции 0;0(относительно currentSize),
      строка с максимальным значением, текущий размер таблицы(size-currentsize))*/
    if (mass[j][j] != maxVal)
    {
        float *vect = new float[size];                                                      //вектор для замены строк
        
        for (int i = 0; i < size; i++)                                                      //присваивание вектору строки с максимальным числом
        {
            vect[i] = mass[j][i];
            mass[j][i] = mass[lineVal][i];
            mass[lineVal][i] = vect[i];
        }
        
        delete[] vect;
    }
}

void fillCoofMassL(int *ptr_size, float **ptr_massL, float **ptr_massU, size_t j)           //вычисл. кооф. матрицы L
{
    for (size_t i = j; i < *ptr_size; i++)
    {
        if ((i != j) && (j != *ptr_size))
        {
            ptr_massL[i][j] = ptr_massU[i][j] / ptr_massU[j][j];
        }
    }
}

void fillMassU(int *ptr_size, float **ptr_massU, float **ptr_massL, size_t j)               //замена элементов U матрицы
{
    for (size_t i = j + 1; i < *ptr_size; i++)
    {
        for (size_t k = j; k < *ptr_size; k++)
        {
            ptr_massU[i][k] = ptr_massU[i][k] - ptr_massU[j][k] * ptr_massL[i][j];
        }
    }
}
